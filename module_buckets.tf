module "buckets" {

    # Path
    source = "./dir_module_buckets"

    # Variables
    bucket_names = ["gold", "silver", "bronze", "scripts"]
}