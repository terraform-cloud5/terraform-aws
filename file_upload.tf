resource "aws_s3_object" "jars" {
    bucket = "bkt-${var.project_name}-scripts-${var.enviroment}"
    key = "./jars/delta-core_2.12-1.0.0.jar"
    source = "./jars/delta-core_2.12-1.0.0.jar"
    etag = filemd5("jars/delta-core_2.12-1.0.0.jar")
    depends_on = [aws_s3_bucket.bucket-stack]
}

resource "aws_s3_object" "jobGlue" {
    bucket = "bkt-${var.project_name}-scripts-${var.enviroment}"
    key = "job/jobGlue.py"
    source = "job/jobGlue.py"
    etag = filemd5("job/jobGlue.py")
    depends_on = [aws_s3_bucket.bucket-stack]
}