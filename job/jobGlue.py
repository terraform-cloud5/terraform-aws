import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, TimestampType, FloatType
from pyspark.sql.window import Window
from pyspark.sql.function import col, row_number
from pyspark.sql.function import *
from delta import *

builder = pyspark.sql.SparkSession.builder.appName("MyApp") \
    .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
    .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")

spark = configure_spark_with_delta_pip(builder).getOrCreate()
spark.sparkContex.setLogLevel("ERROR")

# Config
people_path = "s3a://bkt-data-platform-bronze-dev/people_table"
emp_path = "bkt-data-platform-bronze-dev/emp"
dep_path = "bkt-data-platform-bronze-dev/dep"
dim_path = "bkt-data-platform-bronze-dev/dimension"


# printing table
def show_table(table_path:str) -> None:
    print("\nShow the table: ")
    df = spark.read.format("delta").load(table_path)
    df.show()


def generate_data(table_path:str, table_path2:str, table_path3:str) -> None:

    print("\n creating table: ")
    employees = [
        (1, "Matheus", "", "Silva", "12133", "M", 5000, 1, 2222),
        (2, "Michael", "Rose", "", "667868", "M", 4000, 2, 3333),
        (3, "Robert", "", "Williams", "5465464", "M", 3500, 3, 4444),
        (4, "Maria", "Anne", "Jones", "241424", "F", 3800, 3, 555),
        (5, "Jen", "Mary", "Brown", "", "F", -1, 3, 666),
    ]

    schema_emp = StructType([
        StructField("id", IntegerType(), True),
        StructField("firstname", StringType(), True),
        StructField("middlename", StringType(), True),
        StructField("lastname", StringType(), True),
        StructField("cod_entry", StringType(), True),
        StructField("gender", StringType(), True),
        StructField("salary", IntegerType(), True),
        StructField("DepID", IntegerType(), True),
        StructField("CostCenter", StringType(), True),
    ])

    departments = [
        (1, "IT"),
        (2, "Marketing"),
        (3, "Human Resource"),
        (4, "Sales")
    ]

    schema_dep = StructType([
        StructField("id", IntegerType(), True),
        StructField("Department", StringType(), True)
    ])

    df_emp = spark.createDataFrame(data=employees, schema=schema_emp)
    df_dep = spark.createDataFrame(data=departments, schema=schema_dep)

    print("Saving Delta tables ..\n")
    df_emp.write.forma("delta")\
        .option("overwriteSchema", "true")\
        .mode("overwrite")\
        .save(table_path)

    df_dep.write.forma("delta")\
        .option("overwriteSchema", "true")\
        .mode("overwrite")\
        .save(table_path2)

    print("Loading delta tables.. \n")
    df_emp = spark.read.format("delta").load(table_path)
    df_dep = spark.read.format("delta").load(table_path2)

    df_emp.show()
    df_dep.show()

    print("creating delta views.. \n")
    df_emp.createOrReplaceTempView('employees')
    df_dep.createOrReplaceTempView('departments')

    print("Dimension table .. \n")
    query = spark.sql(
        '''
            SELECT e.id,
                   e.firstname,
                   e.middlename,
                   e.salary,
                   d.Department
            FROM employees e
            INNER JOIN departments d
            ON e.id = d.id
        '''
    )

    query.show()

    print('Saving delta table for dimension table...')
    query.write.format("delta")\
        .mode("overwrite")\
        .save(table_path3)

generate_data(emp_path, dep_path, dim_path)

