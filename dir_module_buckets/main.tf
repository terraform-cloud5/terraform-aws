resource "aws_s3_bucket" "bucket-stack" {
    count           = length(var.bucket_names)
    bucket          = "bkt-${var.project_name}-${var.bucket_names[count.index]}-${var.enviroment}"
    force_destroy   = true

    tags = {
      Bucket_name   = "bkt-${var.project_name}-${var.bucket_names[count.index]}-${var.enviroment}"
      enviroment    = var.enviroment
      Cost_Center   = "TI"
      project_name  = var.project_name
    }
  
}

resource "aws_s3_bucket_server_side_encryption_configuration" "bucket-encryption" {
    count   = length(var.bucket_names)
    bucket  = "bkt-${var.project_name}-${var.bucket_names[count.index]}-${var.enviroment}"
    rule {
        apply_server_side_encryption_by_default {
          sse_algorithm = "AES256"
        }
    }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
    count   = length(var.bucket_names)
    bucket  = "bkt-${var.project_name}-${var.bucket_names[count.index]}-${var.enviroment}"
    acl     = "private"
}

resource "aws_s3_bucket_public_access_block" "block-puplic" {
    count   = length(var.bucket_names)
    bucket  = "bkt-${var.project_name}-${var.bucket_names[count.index]}-${var.enviroment}"

    block_public_acls         = true
    block_public_policy       = true
    ignore_public_acls        = true
    restrict_public_buckets   = true
}